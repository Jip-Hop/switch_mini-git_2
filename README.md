# **Switch mini** #
Forked mlv_dump functions into a smaller more reliable, faster app. On top of that ml user Jip-Hop refined more or less all included scripts into a slimmed down one script solution. 
Mlv_dump version based on ml user bouncyball´s mlv_dump on steroids branch which includes most functions from mlvfs(dmilligan). 
Main reasons releasing Switch mini are to gain processing speed and to make full use of bad pixel fix routines working the same as in mlvfs and Mlv App. Switch mini is also able to perform auto white balancing, correcting multipliers, and pushing them into the AsShotNutral dng tag with a little help from exiv2.


**Switch mini**

![Screen_Shot_2017-07-26_at_10.05.56.png](https://i.postimg.cc/gkYk15sc/Ska-rmavbild-2020-08-24-kl-21-54-47-png-800px.jpg)

![Screen_Shot_2017-07-26_at_10.05.56.png](https://i.postimg.cc/nLxzkNMv/2.jpg)

## HOWTO ##

- Check images

**regarding gatekeeper**

To supress gatekeeper hold ctrl or cmd button down(macOS Sierrra, Mojave, Catalina) while right clicking/opening the application the first time. You can also change permissions from within privacy/security settings.

**Thanks to:** a1ex, g3gg0, bouncyball, dmilligan, Jip-Hop, Dave Coffin(dcraw), Phil Harvey(Exiftool), Andreas Huggel(exiv2), dfort(Focus pixel script)
#Copyright Danne
